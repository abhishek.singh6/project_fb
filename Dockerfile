FROM python:3.6-alpine

RUN adduser -D abhishek

WORKDIR /home/abhishek

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY project.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP project.py

RUN chown -R abhishek:abhishek ./
USER abhishek

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
