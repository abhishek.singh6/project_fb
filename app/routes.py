from flask import render_template
from app import app
from flask import render_template
from app import app
from app.forms import LoginForm, CommentForm
from flask import render_template, flash, redirect
from flask import render_template, flash, redirect, url_for
from flask_login import current_user, login_user
from app.models import User
from flask_login import logout_user
from flask_login import login_required
from datetime import datetime
from app.forms import PostForm
from app.models import Post, Comment
from app.forms import ResetPasswordRequestForm
from app.email import send_password_reset_email
from app.forms import ResetPasswordForm
from flask import request
from werkzeug.urls import url_parse
from app.models import Post, User
from app import db
from app.forms import RegistrationForm
from app.forms import EditProfileForm
from elasticsearch import Elasticsearch

es = Elasticsearch()




POSTS_PER_PAGE = 3



@app.route('/index/<user_id>/<post_id>', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index(user_id=None, post_id=None):
    comments = Comment.query.all()
    form = PostForm()
    comment_form = CommentForm()
    users = User.query.all()
 
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page,POSTS_PER_PAGE , False)
    next_url = url_for('index', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('index', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template('index.html', title='Home', form=form,comment_form = comment_form,users = users,comments = comments, posts=posts.items, next_url=next_url, prev_url=prev_url)
    posts = current_user.followed_posts().all()
    
























@app.route('/public', methods=['GET', 'POST'])
def public_posts():
    page = request.args.get('page', 1, type=int)
    users = User.query.all()
    comments = Comment.query.all()
    posts = Post.query.all()
    return render_template('public_post.html', users = users, posts = posts, comments= comments)























@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)

@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html',
                           title='Reset Password', form=form)



@app.route('/<username>')
@login_required
def profile(username):
    comments = Comment.query.all()
    comment_form = CommentForm()
    users = User.query.all()
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(user_id  = user.id)
    return render_template('profile.html', user=user, posts=posts,comment_form = comment_form,comments = comments )
    
    
    
@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.firstname = form.firstname.data
        current_user.lastname = form.lastname.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.firstname.data = current_user.firstname
        form.lastname.data = current_user.lastname
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile',form=form)

@app.route('/like/<post_id>')
@login_required
def like(post_id):
    post_id = int(post_id)
    post = Post.query.filter_by(id = post_id).first()
    post.like(current_user)
    db.session.add(post)
    db.session.commit()
    flash('You have liked {}!'.format(post.id))
    return redirect(url_for('index'))

@app.route('/dislike/<post_id>')
@login_required
def dislike(post_id):
    post = Post.query.filter_by(id = post_id).first()
    post.dislike(current_user)
    db.session.add(post)
    db.session.commit()
    flash('You have dis-liked post with id {}!'.format(post.id))
    return redirect(url_for('index'))

@app.route('/find_friends', methods=['GET', 'POST'])
@login_required
def find_friends():
    users = User.query.all()
    return render_template("find_friends.html", title='Home Page', users=users)

@app.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot follow yourself!')
        return redirect(url_for('user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('You are following {}!'.format(username))
    return redirect(url_for('profile', username=username))

@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    return redirect(url_for('profile', username=username))

@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page =  request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))